const router = require('express').Router()
const routerUser = require('./user-router')
const routerAdmin = require('./admin-router')
const routerAxios = require('./axios-router')
const routerMovie = require('./movie-router')
const routerReview = require('./review-router')

router.use('/user', routerUser)
router.use('/', routerAdmin)
router.use('/axios', routerAxios)
router.use('/movie', routerMovie)
router.use('/review', routerReview)

module.exports = router