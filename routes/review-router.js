const router = require('express').Router()
const auth = require('../middleware/authenticate')
const review = require('../controller/review-controller')
const ownership = require('../middleware/checkOwnership')

router.post('/new/:movieId', auth, review.create)
router.get('/owned', auth, review.readOwned)
router.get('/:movieId', review.read)
router.put('/:reviewId', auth, ownership('Review'), review.update)
router.delete('/:reviewId', auth, ownership('Review'), review.delete)

module.exports = router