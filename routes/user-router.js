const router = require('express').Router()
const user = require('../controller/user-controller')
const auth =  require('../middleware/authenticate')
const upload = require('../middleware/uploader')
// const axios = require('../controller/test-axios')

router.post('/register', user.register)
router.post('/login', user.login)
router.get('/',auth, user.show)
router.put('/',auth,upload.single('image'), user.update)
// router.get('/omdb/:title', axios.getAPI);
// router.get('/tmdb', axios.getAPI);

module.exports = router