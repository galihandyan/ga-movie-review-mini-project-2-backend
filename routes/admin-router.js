const router = require('express').Router()
const admin = require('../controller/admin-controller')
// const auth =  require('../middleware/authenticate')
// const upload = require('../middleware/uploader')
// const axios = require('../controller/test-axios')

router.get('/', admin.page.home)
router.get('/dashboard', admin.page.dashboard)
router.get('/dashboard/new', admin.page.new)
router.get('/dashboard/update', admin.page.update)

module.exports = router