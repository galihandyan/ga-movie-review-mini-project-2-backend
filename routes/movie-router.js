const router = require('express').Router()
const movie = require('../controller/movie-controller')
const auth = require('../middleware/authenticate')

// Router movie collection

router.get('/', movie.general)
router.get('/upcoming', movie.upcoming)
router.get('/search', movie.search)
router.get('/genre', movie.allGenre)
router.get('/genre/:genreId', movie.byGenre)
router.get('/watchlist',auth, movie.showWatchlist)
router.post('/watchlist/:movieId', auth, movie.addWatchlist)
router.delete('/watchlist/:watchlistId', auth, movie.removeWatchlist)
router.get('/:movieId', movie.detail)
router.get('/:movieId/crew', movie.crew)
router.get('/:movieId/actor', movie.actor)

module.exports = router