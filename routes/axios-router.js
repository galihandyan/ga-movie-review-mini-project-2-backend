const router = require('express').Router()
const axios = require('../controller/axios-controller')

router.get('/popular/:page', axios.getMovie)
router.get('/upcoming/:page', axios.getUpComing)
router.get('/genre', axios.getGenre)
router.get('/trailer', axios.getTrailer)
router.get('/credit', axios.getCredit)

module.exports = router