const ImageKit = require('imagekit')

module.exports = new ImageKit({
  publicKey : process.env.IMGKIT_PUBLIC_APIKEY,
  privateKey : process.env.IMGKIT_PRIVATE_APIKEY,
  urlEndpoint : process.env.IMGKIT_URL
})