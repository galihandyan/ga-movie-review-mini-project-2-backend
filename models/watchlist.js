'use strict'
module.exports = (sequelize, DataTypes) => {
  const Watchlist = sequelize.define('Watchlist', {
    movies_id: DataTypes.INTEGER,
    users_id: DataTypes.INTEGER
  }, {})
  Watchlist.associate = function(models) {
    // associations can be defined here
    Watchlist.belongsTo(models.User,{
      foreignKey: 'users_id'
    })

    Watchlist.belongsTo(models.Movie,{
      foreignKey: 'movies_id'
    })
  }
  return Watchlist
}