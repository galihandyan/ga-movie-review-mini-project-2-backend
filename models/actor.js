'use strict'
module.exports = (sequelize, DataTypes) => {
  const Actor = sequelize.define('Actor', {
    name: DataTypes.STRING,
    character: DataTypes.STRING,
    movies_id: DataTypes.INTEGER,
  }, {})
  Actor.associate = function (models) {
    // associations can be defined here
    Actor.hasOne(models.Image, {
      foreignKey: 'actors_id'
    })

    Actor.belongsTo(models.Movie, {
      foreignKey: 'movies_id'
    })
  }
  return Actor
}