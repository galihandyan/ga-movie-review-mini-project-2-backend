'use strict'
const bcrypt = require('bcryptjs')
const jwt = require('../helper/token')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Name must be filled in'
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Email must be filled in'
        },
        isEmail: {
          args: true,
          msg: 'Email must be email format'
        }
      },
      unique: {
        args: true,
        msg: 'Email already registered'
      }
    },
    password: DataTypes.STRING,
    role: {
      type: DataTypes.STRING,
      defaultValue: 'member'
    }
  }, {
    hooks: {
      // beforeValidate: instance => {
      //   // instance.email = instance.email.toLowerCase();
      // },

      beforeCreate: instance => {
        instance.email = instance.email.toLowerCase()
        instance.password = bcrypt.hashSync(instance.password, 10)
      }
    }
  })
  User.associate = function (models) {
    // associations can be defined here
    User.hasOne(models.Image, {
      foreignKey: 'users_id'
    })

    User.hasMany(models.Review, {
      foreignKey: 'users_id'
    })

    User.hasMany(models.Watchlist, {
      foreignKey: 'users_id'
    })
  }

  User.register = async (data) => {
    const user = await User.create(data)
    const token = jwt(user.id, user.name, user.email)
    return {
      user,
      token
    }
  }

  User.authenticate = async (data) => {
    const {
      email,
      password
    } = data

    const checkEmail = await User.findOne({
      where: {
        email
      }
    })

    if (!checkEmail) throw new Error('Email does not exist')
    const checkPassword = await bcrypt.compareSync(password, checkEmail.password)
    if (!checkPassword) throw new Error('Password or Email incorrect')
    const token = jwt(checkEmail.id, checkEmail.name, checkEmail.email)

    return {
      checkEmail,
      token
    }
  }

  User.edit = async (req, data) => {
    return User.update({
      name: data
    }, {
      where: {
        id: req.token.id
      }
    })
  }

  User.getData = async (data) => {
    const user = await User.findByPk(data)

    return {
      name: user.name,
      email: user.email
    }
  }
  return User
}