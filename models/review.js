'use strict'
module.exports = (sequelize, DataTypes) => {
  const Review = sequelize.define('Review', {
    comment: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Field must be filled'
        }
      }
    },
    rating: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Field must be filled'
        }
      }
    },
    movies_id: DataTypes.INTEGER,
    users_id: DataTypes.INTEGER
  }, {})
  Review.associate = function (models) {
    // associations can be defined here
    Review.belongsTo(models.User, {
      foreignKey: 'users_id'
    })

    Review.belongsTo(models.Movie, {
      foreignKey: 'movies_id'
    })
  }
  return Review
}