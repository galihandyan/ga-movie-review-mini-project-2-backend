'use strict'
module.exports = (sequelize, DataTypes) => {
  const Movie_genre = sequelize.define('Movie_genre', {
    movies_id: DataTypes.INTEGER,
    genres_id: DataTypes.INTEGER
  }, {})
  // Movie_genre.associate = function(models) {
  //   // associations can be defined here
  // }
  return Movie_genre
}