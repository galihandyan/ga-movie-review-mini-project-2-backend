'use strict'
module.exports = (sequelize, DataTypes) => {
  const Image = sequelize.define('Image', {
    url: {
      type: DataTypes.TEXT,
      defaultValue: 'https://ik.imagekit.io/g4nt4prim4ry/sample/BlueHead_JkxqLVWc7.jpg',
      validate: {
        notEmpty: {
          args: true,
          msg: 'Image URL must be filled in'
        }
      }
    },
    users_id: DataTypes.INTEGER,
    movies_id: DataTypes.INTEGER,
    actors_id: DataTypes.INTEGER,
    crews_id: DataTypes.INTEGER,
    description: DataTypes.STRING
  }, {})
  Image.associate = function (models) {
    // associations can be defined here
    Image.belongsTo(models.User, {
      foreignKey: 'users_id'
    })

    Image.belongsTo(models.Movie, {
      foreignKey: 'movies_id'
    })

    Image.belongsTo(models.Actor,{
      foreignKey: 'actors_id'
    })

    Image.belongsTo(models.Crew,{
      foreignKey: 'crews_id'
    })
  }

  Image.newImage = (data) => {
    return Image.create({
      users_id: data,
      description: 'User Image'
    })
  }

  Image.getUrl = (data) => {
    return Image.findOne({
      where: {
        users_id: data
      }
    })
  }

  Image.edit = (req, data) => {
    return Image.update({
      url: data
    }, {
      where: {
        users_id: req.token.id
      }
    })
  }
  return Image
}