'use strict'
module.exports = (sequelize, DataTypes) => {
  const Crew = sequelize.define('Crew', {
    name: DataTypes.STRING,
    department: DataTypes.STRING,
    job: DataTypes.STRING,
    movies_id: DataTypes.INTEGER,
  }, {})
  Crew.associate = function(models) {
    // associations can be defined here
    Crew.hasOne(models.Image, {
      foreignKey: 'crews_id'
    })

    Crew.belongsTo(models.Movie, {
      foreignKey: 'movies_id'
    })
  }
  return Crew
}