'use strict'
module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define('Movie', {
    title: DataTypes.STRING,
    synopsis: DataTypes.TEXT,
    releaseDate: DataTypes.DATE,
    status: DataTypes.STRING
  }, {})
  Movie.associate = function (models) {
    Movie.belongsToMany(models.Genre, {
      foreignKey: 'movies_id',
      through: {
        model: models.Movie_genre,
        unique: false
      }
    })

    Movie.hasMany(models.Watchlist, {
      foreignKey: 'movies_id'
    })

    Movie.hasMany(models.Image, {
      foreignKey: 'movies_id'
    })

    Movie.hasMany(models.Video, {
      foreignKey: 'movies_id'
    })

    Movie.hasMany(models.Crew, {
      foreignKey: 'movies_id'
    })

    Movie.hasMany(models.Actor, {
      foreignKey: 'movies_id'
    })

    Movie.hasMany(models.Review, {
      foreignKey: 'movies_id'
    })
  }
  return Movie
}