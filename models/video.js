'use strict'
module.exports = (sequelize, DataTypes) => {
  const Video = sequelize.define('Video', {
    url: DataTypes.TEXT,
    movies_id: DataTypes.INTEGER
  }, {})
  Video.associate = function(models) {
    // associations can be defined here
    Video.belongsTo(models.Movie,{
      foreignKey: 'movies_id'
    })
  }
  return Video
}