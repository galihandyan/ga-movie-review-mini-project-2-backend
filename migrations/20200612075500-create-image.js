'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Images', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      url: {
        type: Sequelize.TEXT
      },
      users_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Users',
            schema: 'public'
          },
          key: "id"
        }
      },
      movies_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Movies',
          schema: 'public'
        },
        key: 'id'
      },
      actors_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Actors',
          schema: 'public'
        },
        key: 'id'
      },
      crews_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Crews',
          schema: 'public'
        },
        key: 'id'
      },
      description: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Images');
  }
};