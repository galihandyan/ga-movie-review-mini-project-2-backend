'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Watchlists', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      movies_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Movies',
          schema: 'public'
        },
        key: 'id'
      },
      users_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          schema: 'public'
        },
        key: 'id'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Watchlists');
  }
};