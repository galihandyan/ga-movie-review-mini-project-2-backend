'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Movie_genres', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      movies_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Movies',
          schema: 'public'
        },
        key: 'id'
      },
      genres_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Genres',
          schema: 'public'
        },
        key: 'id'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Movie_genres');
  }
};