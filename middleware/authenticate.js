const jwt = require('jsonwebtoken')
const {
  User
} = require('../models')
require('dotenv').config()
const error = require('../helper/error-handler')

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.auth
    let payload = await jwt.verify(token, process.env.SECRET_KEY)

    await User.findByPk(payload.id)
    req.token = payload
    next()
  } catch (err) {
    error(res, 403, 'You do not have permission!')
  }
}