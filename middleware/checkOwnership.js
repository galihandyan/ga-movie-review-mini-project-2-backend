const models = require('../models')
module.exports = (modelName) => {
  const Model = models[modelName]

  return async function (req, res, next) {
    try {
      let instance = await Model.findByPk(req.params.reviewId)

      if (instance.users_id !== req.token.id) {
        res.status(403).json({
          status:'fail',
          message: `This is not your ${modelName}`
        })
      }

      next()
    } catch (err) {
      res.status(400).json({
        status:'fail',
        message: err.message
      })
    }
  }
}