module.exports = [
  (req, res, next) => {
    res.status(404)
    next(new Error('Page Not Found!!!!!!!!!'))
  },
  
  (err, req, res) => {
    if (res.statusCode == 200) res.status(500)
  
    res.status(res.statusCode).json({
      status: 'fail',
      message: err.message
    })
  }
]