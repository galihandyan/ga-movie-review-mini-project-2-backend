const request = require('supertest');
const app = require('../server');
const db = require('../models');
const {
    Image,
    User
} = require('../models');
const jwt = require('../helper/token');
let token;

describe('Test User API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query(`TRUNCATE "Users", "Images", "Movies", "Genres", "Movie_genres", 
        "Actors", "Crews", "Videos","Watchlists", "Reviews" RESTART IDENTITY`);
        User.create({
            name: 'Tester User',
            email: 'tester_email@mail.com',
            password: 'password'
        }).then(user => {
            token = jwt(user.dataValues.id, user.dataValues.name, user.dataValues.email);
            Image.create({
                users_id: user.dataValues.id
            })
            done()
        })
    })
    afterAll((done)=>{
        db.sequelize.query(`TRUNCATE "Users", "Images", "Movies", "Genres", "Movie_genres", 
        "Actors", "Crews", "Videos","Watchlists", "Reviews" RESTART IDENTITY`);
        done()
    })

    describe('GET /api/v1/user/', () => {
        test('Success show profile. Status code 200', done => {
            request(app)
                .get('/api/v1/user')
                .set('auth', token)
                .set('Content-Type', 'application/json')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /api/v1/user/update', () => {
        test('Success updated profile. Status code 200', done => {
            request(app)
                .put('/api/v1/user')
                .set('auth', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/sample_image/BlueHead.jpg')
                .field({
                    name: "Task Tester Terbaru"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed updated profile. Status code 422', done => {
            request(app)
                .put('/api/v1/user/')
                .set('auth', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/sample_image/BlueHead.jpg')
                .field({
                    name: ""
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('POST /api/v1/user/register', () => {
        test('Success register. Status code 201', done => {
            request(app)
                .post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'another user tester',
                    email: 'another@gmail.com',
                    password: 'another'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed register. Status code 400', done => {
            request(app)
                .post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'another user tester',
                    email: 'another@gmail.com',
                    password: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('POST /api/v1/user/login', () => {
        test('Success login. Status code 202', done => {
            request(app)
                .post('/api/v1/user/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'tester_email@mail.com',
                    password: 'password'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(202);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed login. Status code 401', done => {
            request(app)
                .post('/api/v1/user/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'Tester@email.com',
                    password: '123'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})