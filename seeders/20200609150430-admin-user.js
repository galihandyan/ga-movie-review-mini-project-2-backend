'use strict'
const bcrypt = require('bcryptjs')
module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Users', [{
      name: 'admin',
      email: 'admin@email.com',
      password: bcrypt.hashSync('admin', 10),
      role: 'admin',
      createdAt: new Date,
      updatedAt: new Date
    }], {})
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Users',null,{})
  }
}