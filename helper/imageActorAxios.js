const {
  Image
} = require('../models')

module.exports = async (actorURL, actorId, next) => {
  if (actorURL !== null) {
    await Image.create({
      url: `http://image.tmdb.org/t/p/w200${actorURL}`,
      actors_id: actorId,
      description: 'Actor Image'
    })
  } else {
    await Image.create({
      actors_id: actorId,
      description: 'Actor Image'
    })
  }
  next()
}