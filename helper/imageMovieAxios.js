const {
  Image
} = require('../models')

module.exports = async (poster, backdrop, movieId, next) => {
  await Image.create({
    url: `http://image.tmdb.org/t/p/w200${poster}`,
    movies_id: movieId,
    description: 'Poster Movie Small'
  })

  await Image.create({
    url: `http://image.tmdb.org/t/p/w342${poster}`,
    movies_id: movieId,
    description: 'Poster Movie Large'
  })
  
  await Image.create({
    url: `http://image.tmdb.org/t/p/w500${backdrop}`,
    movies_id: movieId,
    description: 'Backdrop Movie Small'
  })

  await Image.create({
    url: `http://image.tmdb.org/t/p/w1280${backdrop}`,
    movies_id: movieId,
    description: 'Backdrop Movie Large'
  })

  next()
}