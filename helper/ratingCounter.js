const {
  Review
} = require('../models')

module.exports = async (movieId) => {
  let average = 0
  let finalAverage = 0
  const rating = await Review.findAndCountAll({
    attributes: ['rating'],
    where: {
      movies_id: movieId
    }
  })

  if (rating.count > 0) {
    rating.rows.forEach(i => {
      if (i.rating === null) i.rating = 0
      average = average + i.rating
    })
    average = average / rating.count
    finalAverage = average.toFixed(1)
  }
  return finalAverage
}