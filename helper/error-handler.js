module.exports = (res, code, err) => {
  res.status(code).json({
    status: 'fail',
    message: err
  })
}