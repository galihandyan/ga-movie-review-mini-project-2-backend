const imagekit = require('../lib/imagekit')

module.exports = (buffer) => {
  return imagekit.upload({
    file: buffer,
    fileName: 'IMG_' + Date.now()
  })
}