const {
  Image
} = require('../models')

module.exports = async (crewURL, crewId, next) => {
  if (crewURL !== null) {
    await Image.create({
      url: `http://image.tmdb.org/t/p/w200${crewURL}`,
      crews_id: crewId,
      description: 'Crew Image'
    })
  } else {
    await Image.create({
      crews_id: crewId,
      description: 'Crew Image'
    })
  }
  next()
}