require('dotenv').config()
const jwt = require('jsonwebtoken')


module.exports = (id,name,email) => {
  const token = jwt.sign({
    id: id,
    email: email,
    name: name
  }, process.env.SECRET_KEY)
  return token
}