require('dotenv').config()
const express = require('express')
const app = express()
const morgan = require('morgan')
const cors = require('cors')
const router = require('./routes/main-router')
const errorHandler = require('./middleware/error-handler')

app.set('view engine', 'pug')
app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))
app.use(cors())

if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'))

app.use('/api/v1', router)
errorHandler.forEach(handler => app.use(handler))

module.exports = app