const axios = require('axios')
const {
  Genre,
  Movie,
  Movie_genre,
  Video,
  Actor,
  Crew
} = require('../models')
const respon = require('../helper/succes-handler')
const imageMovie = require('../helper/imageMovieAxios')
const imageActor = require('../helper/imageActorAxios')
const imageCrew = require('../helper/imageCrewAxios ')
const tmdbAPI = process.env.TMDB_API


module.exports = {
  getMovie: async (req, res, next) => {
    try {
      const page = req.params.page

      // Hit API TMDB to get popular movie information
      const movie = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${tmdbAPI}&language=en-US&page=${page}`)

      // Store the data that we need to the Movie table
      await movie.data.results.forEach(async (i) => {
        const data = await Movie.create({
          id: i.id,
          title: i.original_title,
          synopsis: i.overview,
          releaseDate: i.release_date,
          status: 'released'
        })

        // Call helper to store poster and backdrop image url for each movie to the Image table
        await imageMovie(i.poster_path, i.backdrop_path, data.id, next)

        // store the Movie id and Genre id to the Movie_genre table as a junction table
        i.genre_ids.forEach(j => {
          Movie_genre.create({
            movies_id: data.id,
            genres_id: j
          })
        })
      })
      respon(res, 200, movie.data)
    } catch (err) {
      res.status(200)
      next(err)
    }
  },

  getUpComing: async (req, res, next) => {
    try {
      const page = req.params.page

      // Hit API TMDB to get popular movie information
      const movie = await axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=${tmdbAPI}&language=en-US&page=${page}`)

      // Store the data that we need to the Movie table
      await movie.data.results.forEach(async (i) => {
        const data = await Movie.create({
          id: i.id,
          title: i.original_title,
          synopsis: i.overview,
          status: 'up coming'
        })

        // Call helper to store poster and backdrop image url for each movie to the Image table
        await imageMovie(i.poster_path, i.backdrop_path, data.id, next)

        // store the Movie id and Genre id to the Movie_genre table as a junction table
        i.genre_ids.forEach(j => {
          Movie_genre.create({
            movies_id: data.id,
            genres_id: j
          })
        })
      })
      respon(res, 200, movie.data)
    } catch (err) {
      res.status(200)
      next(err)
    }
  },

  getTrailer: async (req, res) => {
    const movie = await Movie.findAll()
    movie.forEach(async (i) => {
      const id = i.dataValues.id

      // Hit API TMDB to get trailer for each movie, then store the url to the Video table
      const trailer = await axios.get(`https://api.themoviedb.org/3/movie/${id}/videos?api_key=${tmdbAPI}&language=en-US`)
      trailer.data.results.forEach(o => {
        Video.create({
          url: `https://www.youtube.com/watch?v=${o.key}`,
          movies_id: i.id
        })
      })
    })
    respon(res, 200, 'ok')
  },

  getCredit: async (req, res, next) => {
    try {
      const movie = await Movie.findAll()
      movie.forEach(async (i) => {
        const id = i.dataValues.id

        // Hit API TMDB get all credit (cast and crew)
        const credit = await axios.get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=${tmdbAPI}`)

        //Get cast data then store to Actor table
        credit.data.cast.forEach(async (c) => {
          const actorURL = c.profile_path
          const actor = await Actor.create({
            name: c.name,
            character: c.character,
            movies_id: id
          })
          await imageActor(actorURL, actor.dataValues.id, next)
        })

        credit.data.crew.forEach(async (j) => {
          const crewURL = j.profile_path
          const crew = await Crew.create({
            name: j.name,
            department: j.department,
            job: j.job,
            movies_id: id
          })
          await imageCrew(crewURL, crew.dataValues.id, next)
        })
      })
      respon(res, 200, 'masuk')
    } catch (err) {
      res.status(422)
      next(err)
    }
  },

  getGenre: async (req, res, next) => {
    try {
      // Hit TMDB API to get genre data, then store toGenre table
      const genre = await axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=${tmdbAPI}&language=en-US`)
      await genre.data.genres.forEach(i => {
        Genre.create({
          id: i.id,
          name: i.name
        })
      })

      respon(res, 200, genre.data)
    } catch (err) {
      next(err)
    }
  }
}