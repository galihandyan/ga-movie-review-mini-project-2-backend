const {
  Review,
  User,
  Image,
  Movie
} = require('../models')
const success = require('../helper/succes-handler')
const error = require('../helper/error-handler')
const rating = require('../helper/ratingCounter')
const Op = require('sequelize').Op

module.exports = {
  create: async (req, res) => {
    try {
      const duplicateReview = await Review.findOne({
        where: {
          [Op.and]: [{
            users_id: req.token.id
          }, {
            movies_id: req.params.movieId
          }]
        }
      })

      if (duplicateReview) throw new Error('You only can give review one time!')

      const review = await Review.create({
        comment: req.body.comment,
        rating: req.body.rating,
        users_id: req.token.id,
        movies_id: req.params.movieId
      })

      success(res, 201, review)
    } catch (err) {
      error(res, 422, err.message)
    }
  },

  read: async (req, res) => {
    try {
      const review = await Review.findAndCountAll({
        where: {
          movies_id: req.params.movieId
        },
        order: [
          ['createdAt', 'DESC']
        ],
        limit: req.query.limit || 10,
        offset: (req.query.page - 1) * req.query.limit || 0,
        include: [{
          model: User,
          attributes: ['id', 'name', 'email'],
          include: [{
            model: Image,
            attributes: ['url']
          }]
        }]
      })
      let average = await rating(req.params.movieId)
      const data = {
        review,
        average
      }
      success(res, 200, data)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  readOwned: async (req, res) => {
    try {
      const review = await Review.findAndCountAll({
        where: {
          users_id: req.token.id
        },
        order: [
          ['createdAt', 'DESC'],
          [Movie, Image, 'description', 'DESC']
        ],
        include: [{
          model: Movie,
          attributes: ['id', 'title'],
          include: [{
            model: Image,
            attributes: ['url','description']
          }]
        }]
      })
      success(res, 200, review)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  update: async (req, res) => {
    try {
      await Review.update(req.body, {
        where: {
          id: req.params.reviewId
        }
      })

      const message = `Review with ID ${req.params.reviewId} has been updated`
      success(res, 200, message)
    } catch (err) {
      error(res, 422, err.message)
    }
  },

  delete: async (req, res) => {
    try {
      await Review.destroy({
        where: {
          id: req.params.reviewId
        }
      })
      const message = `Review with ID ${req.params.reviewId} has been deleted`
      success(res, 200, message)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

}