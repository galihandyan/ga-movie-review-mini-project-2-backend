const {
  Movie,
  Genre,
  Image,
  Video,
  Crew,
  Actor,
  Watchlist
} = require('../models')
const rating = require('../helper/ratingCounter')
const success = require('../helper/succes-handler')
const error = require('../helper/error-handler')
const {
  Op
} = require('sequelize')

module.exports = {
  general: async (req, res) => {
    try {
      const data = await Movie.findAll({
        where: {
          status: 'released'
        },
        order: [
          [Image, 'description', 'DESC']
        ],
        attributes: ['id', 'title', 'synopsis', 'releaseDate'],
        limit: req.query.limit || 10,
        offset: (req.query.page - 1) * req.query.limit || 0,
        include: [{
            model: Genre,
            attributes: ['name', ],
            through: {
              attributes: []
            }
          },
          {
            model: Image,
            attributes: ['url', 'description']
          },
        ]
      })
      success(res, 200, data)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  detail: async (req, res) => {
    try {
      const detail = await Movie.findByPk(req.params.movieId, {
        attributes: ['id', 'title', 'synopsis', 'releaseDate'],
        order: [
          [Image, 'description', 'DESC']
        ],
        include: [{
            model: Genre,
            attributes: ['name', ],
            through: {
              attributes: []
            }
          },
          {
            model: Image,
            attributes: ['url', 'description'],
          },
          {
            model: Video,
            attributes: ['url']
          },
          // {
          //   model: Crew,
          //   attributes: ['name', 'department', 'job'],
          //   where: {
          //     job: 'Producer'
          //   }
          // },
          {
            model: Actor,
            attributes: ['name', 'character'],
            limit: 4
          }
        ]
      })
      let average = await rating(req.params.movieId)
      const data = {
        detail,
        average
      }
      success(res, 200, data)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  search: async (req, res) => {
    try {
      const movie = await Movie.findAll({
        where: {
          title: {
            [Op.iLike]: `%${req.query.title}%`
          },
          status: 'released'
        },
        order: [
          [Image, 'description', 'DESC']
        ],
        attributes: ['id', 'title', 'synopsis'],
        include: [{
            model: Genre,
            attributes: ['name', ],
            through: {
              attributes: []
            }
          },
          {
            model: Image,
            attributes: ['url', 'description']
          },
        ]
      })
      success(res, 200, movie)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  upcoming: async (req, res) => {
    try {
      const movie = await Movie.findAll({
        where: {
          status: 'up coming'
        },
        order: [
          [Image, 'description', 'DESC']
        ],
        limit: req.query.limit || 10,
        offset: (req.query.page - 1) * req.query.limit || 0,
        attributes: ['id', 'title', 'synopsis'],
        include: [{
            model: Genre,
            attributes: ['name', ],
            through: {
              attributes: []
            }
          },
          {
            model: Image,
            attributes: ['url', 'description']
          },
        ]
      })
      success(res, 200, movie)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  allGenre: async (req, res) => {
    try {
      const genre = await Genre.findAll({
        attributes: ['id', 'name']
      })
      success(res, 200, genre)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  byGenre: async (req, res) => {
    try {
      const movie = await Genre.findByPk(req.params.genreId, {
        where: {
          status: 'released'
        },
        attributes: ['id', 'name'],
        order: [
          [Movie, Image, 'description', 'DESC']
        ],
        include: {
          model: Movie,
          attributes: ['id', 'title', 'releaseDate'],
          through: {
            attributes: []
          },
          include: {
            model: Image,
            attributes: ['url', 'description']
          }
        }
      })
      success(res, 200, movie)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  crew: async (req, res) => {
    try {
      const crew = await Crew.findAll({
        where: {
          movies_id: req.params.movieId,
        },
        attributes: ['name', 'department', 'job'],
        include: {
          model: Image,
          attributes: ['url']
        }
      })
      success(res, 200, crew)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  actor: async (req, res) => {
    try {
      const actor = await Actor.findAll({
        where: {
          movies_id: req.params.movieId
        },
        attributes: ['name', 'character'],
        include: {
          model: Image,
          attributes: ['url']
        }
      })
      success(res, 200, actor)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  addWatchlist: async (req, res) => {
    try {
      const duplicateWatchlist = await Watchlist.findOne({
        where: {
          [Op.and]: [{
            users_id: req.token.id
          }, {
            movies_id: req.params.movieId
          }]
        }
      })

      if (duplicateWatchlist) throw new Error('You already add this movie to watch list!')

      await Watchlist.create({
        movies_id: req.params.movieId,
        users_id: req.token.id
      })
      const message = `Movie with ID ${req.params.movieId} has been added to your watchlist`
      success(res, 201, message)
    } catch (err) {
      error(res, 422, err.message)
    }
  },

  removeWatchlist: async (req, res) => {
    try {
      await Watchlist.destroy({
        where: {
          id: req.params.watchlistId
        }
      })
      const message = `Watch list with ID ${req.params.watchlistId} has been removed`
      success(res, 200, message)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  showWatchlist: async (req, res) => {
    try {
      const watchlist = await Watchlist.findAll({
        where: {
          users_id: req.token.id
        },
        order: [
          [Movie, Image, 'description', 'DESC']
        ],
        attributes: ['id'],
        include: [{
          model: Movie,
          attributes: ['id', 'title'],
          include: [{
            model: Genre,
            attributes: ['name'],
            through: {
              attributes: []
            }
          }, {
            model: Image,
            attributes: ['url', 'description']
          }]
        }]
      })
      success(res, 200, watchlist)
    } catch (err) {
      error(res, 500, err.message)
    }
  }
}