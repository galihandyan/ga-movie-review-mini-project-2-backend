const {
  User
} = require('../models')
// const respon = require('../helper/token')

module.exports = {
  page: {
    home: (req, res) => {
      res.render('index', {
        title: 'Admin',
        message: 'Login Page'
      })
    },

    dashboard: (req, res) => {
      res.render('movie/index', {
        title: 'Dashboard',
      })
    },

    new: (req, res) => {
      res.render('movie/new',{
        title: 'Create new movie'
      })
    },

    update: (req,res) => {
      res.render('movie/update',{
        title: 'Update movie info'
      })
    }

  },

  create: async (req) => {
    User.create(req.body)
  }
}