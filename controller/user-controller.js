const {
  User,
  Image
} = require('../models')
const respon = require('../helper/succes-handler')
const imagekit = require('../helper/imagekit')
const error = require('../helper/error-handler')

module.exports = {
  register: async (req, res) => {
    try {
      const user = await User.register(req.body)
      const image = await Image.newImage(user.user.id)
      const data = {
        id: user.user.id,
        name: user.user.name,
        email: user.user.email,
        image: image.url,
        token: user.token
      }
      respon(res, 201, data)
    } catch (err) {
      error(res, 422, err.message)
    }
  },

  login: async (req, res) => {
    try {
      const user = await User.authenticate(req.body)
      const image = await Image.getUrl(user.checkEmail.id)

      const data = {
        id: user.checkEmail.id,
        name: user.checkEmail.name,
        email: user.checkEmail.email,
        image: image.url,
        token: user.token
      }
      respon(res, 202, data)
    } catch (err) {
      error(res, 401, err.message)
    }
  },

  show: async (req, res) => {
    try {
      const image = await Image.getUrl(req.token.id)
      const user = await User.getData(req.token.id)
      const data = {
        id: req.token.id,
        name: user.name,
        email: user.email,
        image: image.url
      }
      respon(res, 200, data)
    } catch (err) {
      error(res, 500, err.message)
    }
  },

  update: async (req, res) => {
    try {
      await User.edit(req, req.body.name)
      // console.log(req.file);
      // if(req.file){
      // }
      if (req.file) {
        const image = await imagekit(req.file.buffer)
        await Image.edit(req, image.url)
      }
      const data = `Profile with ID ${req.token.id} has been update`
      respon(res, 200, data)
    } catch (err) {
      error(res, 422, err.message)
    }
  }

}